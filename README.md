# autoRotate

autoRotate is a C++ custom Maya MPxNode that calculates the rotation in degrees based on world space delta movement, radius, global scale and movement axis.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* [CMake](https://cmake.org/) - Cross Platform compilation

### Generate Project

In order to generate the project for your favourite IDE.

* Create a folder called "build" inside the project folder
* Open a terminal and move to the recently created "build" folder
* Execute the line ```cmake -G "Visual Studio 15 2017" -DMAYA_VERSION=2018 ..```

### Compile

In order to compile from the terminal

* Follow the instructions of **Generate Project**
* Execute the line ```cmake --build . --config Release``` or ```cmake --build . --config Debug```

## Authors

* **Gorka Mendieta** [LinkedIn](https://www.linkedin.com/in/gorkamendieta/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

