#include "autoRotate.h"

#include <maya/MFnPlugin.h>

MStatus initializePlugin( MObject obj )
{
	MFnPlugin plugin( obj, "Gorka Mendieta", "2018", "Any");
	MStatus status = plugin.registerNode("autoRotate", AutoRotate::typeId, AutoRotate::creator, AutoRotate::initialize );
	if (!status) {
		status.perror("Error initializing plugin autoRotate");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
{
	MFnPlugin plugin(obj);
	MStatus status = plugin.deregisterNode(AutoRotate::typeId);
	if (!status) {
		status.perror("Error uninitializing plugin autoRotate");
		return status;
	}

	return status;
}
