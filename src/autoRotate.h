
#ifndef AUTO_ROTATE_H
#define AUTO_ROTATE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 
#include <maya/MVector.h>

class AutoRotate : public MPxNode
{

public:
	static MTypeId typeId;
	static MObject in_active;
	static MObject in_scale;  // Global scale
	static MObject in_radius;  // Wheel radius
	static MObject in_inv_rot;  // Invert Rotation
	static MObject in_aim_vec;  // Control direction vector
	static MObject in_driver_matrix;  // World space matrix of the control
	static MObject out_rot;  // Output Rotation

public:
	AutoRotate();
	virtual	~AutoRotate();

	MStatus compute( const MPlug& plug, MDataBlock& data) override;
	static  void* creator();
	static  MStatus initialize();

private:
	MVector prev_translation;
	float prev_rotation;
	bool prev_active;

};

#endif
