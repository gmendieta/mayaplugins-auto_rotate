#include "autoRotate.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MGlobal.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMesh.h>
#include <maya/MBoundingBox.h>
#include <maya/MMatrix.h>
#include <maya/MVector.h>

// Macro to check status
#define CheckStatus(status, message)	\
	if (status != MStatus::kSuccess) {	\
		MGlobal::displayError(message); \
		return status;					\
	}

MTypeId AutoRotate::typeId(0x001295c9);  // Registered Id for Minininja
MObject AutoRotate::in_active;
MObject AutoRotate::in_scale;  // Global scale
MObject AutoRotate::in_radius;  // Wheel radius
MObject AutoRotate::in_inv_rot;  // Invert Rotation
MObject AutoRotate::in_aim_vec;  // Control direction vector
MObject AutoRotate::in_driver_matrix;  // World space matrix of the control
MObject AutoRotate::out_rot;  // Output Rotation

AutoRotate::AutoRotate() {
	prev_translation = MVector::zero;
	prev_rotation = 0.0;
	prev_active = false;
}

AutoRotate::~AutoRotate() {}


void* AutoRotate::creator()
{
	return new AutoRotate();
}

MStatus AutoRotate::initialize()

{
	MStatus status;

	MFnNumericAttribute num_attr;
	MFnMatrixAttribute matrixAttr;

	out_rot = num_attr.create("rotation", "rotation", MFnNumericData::kFloat, 1.0);
	num_attr.setWritable(true);
	status = addAttribute(out_rot);
	CheckStatus(status, "Error adding 'rotation' attribute");

	in_active = num_attr.create("active", "active", MFnNumericData::kBoolean, false);
	num_attr.setStorable(true);
	num_attr.setKeyable(true);
	num_attr.setChannelBox(true);
	status = addAttribute(in_active);
	CheckStatus(status, "Error adding 'active' attribute");

	in_inv_rot = num_attr.create("invertRotation", "invertRotation", MFnNumericData::kBoolean, false);
	num_attr.setStorable(true);
	num_attr.setKeyable(true);
	num_attr.setChannelBox(true);
	status = addAttribute(in_inv_rot);
	CheckStatus(status, "Error adding 'invertRotation' attribute");

	in_scale = num_attr.create("scale", "scale", MFnNumericData::kFloat, 1.0);
	num_attr.setMin(0.01);  // Prevent DivisionByZero
	num_attr.setStorable(true);
	num_attr.setKeyable(true);
	num_attr.setChannelBox(true);
	status = addAttribute(in_scale);
	CheckStatus(status, "Error adding 'scale' attribute");

	in_radius = num_attr.create("radius", "radius", MFnNumericData::kFloat, 1.0);
	num_attr.setMin(0.01);  // Prevent DivisionByZero
	num_attr.setStorable(true);
	num_attr.setKeyable(true);
	num_attr.setChannelBox(true);
	status = addAttribute(in_radius);
	CheckStatus(status, "Error adding 'radius' attribute");

	in_aim_vec = num_attr.createPoint("aimVector", "aimVector");
	num_attr.setDefault(1.0, 0.0, 0.0);
	num_attr.setStorable(true);
	num_attr.setKeyable(true);
	num_attr.setChannelBox(true);
	status = addAttribute(in_aim_vec);
	CheckStatus(status, "Error adding 'aimVector' attribute");

	in_driver_matrix = matrixAttr.create("driverMatrix", "driverMatrix");
	status = addAttribute(in_driver_matrix);
	CheckStatus(status, "Error adding 'driverMatrix' attribute");

	attributeAffects(in_active, out_rot);
	attributeAffects(in_scale, out_rot);
	attributeAffects(in_radius, out_rot);
	attributeAffects(in_inv_rot, out_rot);
	attributeAffects(in_aim_vec, out_rot);
	attributeAffects(in_driver_matrix, out_rot);

	return MS::kSuccess;
}

MStatus AutoRotate::compute( const MPlug& plug, MDataBlock& data )
{
	if (plug != out_rot) {
		return MS::kUnknownParameter;
	}
	MStatus status;
	auto h_active = data.inputValue(in_active, &status);
	CheckStatus(status, "Error getting 'active' handle");
	auto active = h_active.asBool();
	if (active == false) {
		prev_active = false;
		return MS::kSuccess;
	}
	auto h_scale = data.inputValue(in_scale, &status);
	CheckStatus(status, "Error getting 'scale' handle");
	auto h_radius = data.inputValue(in_radius, &status);
	CheckStatus(status, "Error getting 'radius' handle");
	auto h_inv_rot = data.inputValue(in_inv_rot, &status);
	CheckStatus(status, "Error getting 'invertRotation' handle");
	auto h_aim_vec = data.inputValue(in_aim_vec, &status);
	CheckStatus(status, "Error getting 'aimVector' handle");
	auto h_driver_matrix = data.inputValue(in_driver_matrix, &status);
	CheckStatus(status, "Error getting 'driverMatrix' handle");
	auto h_out_rot = data.outputValue(out_rot, &status);
	CheckStatus(status, "Error getting 'rotation' handle");

	const auto scale = h_scale.asFloat();
	const auto radius = h_radius.asFloat();
	const auto inv_rot = h_inv_rot.asBool();
	const auto aim_vec = MVector(h_aim_vec.asFloatVector());
	const auto driver_matrix = data.inputValue(in_driver_matrix).asMatrix();
	const auto aim_vec_ws = aim_vec * driver_matrix;
	const auto translation_ws = MTransformationMatrix(driver_matrix).getTranslation(MSpace::kWorld);
	if (prev_active == false)
		prev_translation = translation_ws;
	const auto move_vec = translation_ws - prev_translation;
	// Projection of move_vec on aim_vec_ws
	const auto move_delta = move_vec * aim_vec_ws; // Dot Product
	float rotation;
	if (inv_rot == true)
		rotation = prev_rotation - move_delta * 360 / (2 * M_PI * radius * scale);
	else
		rotation = prev_rotation + move_delta * 360 / (2 * M_PI * radius * scale);

	prev_rotation = rotation;
	prev_translation = translation_ws;

	h_out_rot.setFloat(rotation);
	prev_active = true;
	data.setClean(plug);
	return MS::kSuccess;
}


