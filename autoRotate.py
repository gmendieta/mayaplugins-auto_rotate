"""

"""

import sys
import math

import maya.OpenMaya as OpenMaya
import maya.OpenMayaMPx as OpenMayaMPx

kPluginName = 'autoRotate'
kPluginAuthor = 'Gorka Mendieta'
kPluginVersion = '0.1'
kPluginId = OpenMaya.MTypeId(0x001295c9)  # Registered for minininja


class AutoRotate(OpenMayaMPx.MPxNode):
    in_active = OpenMaya.MObject()
    in_scale = OpenMaya.MObject()  # Global Scale
    in_radius = OpenMaya.MObject()  # Wheel Radius
    in_inv_rot = OpenMaya.MObject()  # Invert Rotation
    in_aim_vec = OpenMaya.MObject()  # Wheel Direction Vector
    in_driver_matrix = OpenMaya.MObject()  # World space matrix of the control
    in_parent_matrix = OpenMaya.MObject()  # World Space Matrix of the parent of the
    out_rot = OpenMaya.MObject()

    @staticmethod
    def creator():
        return AutoRotate()

    @staticmethod
    def initialize():
        num_attr = OpenMaya.MFnNumericAttribute()
        matrix_attr = OpenMaya.MFnMatrixAttribute()

        AutoRotate.out_rot = num_attr.create('rotation', 'rotation', OpenMaya.MFnNumericData.kFloat, 1.0)
        num_attr.writable = True
        AutoRotate.addAttribute(AutoRotate.out_rot)

        AutoRotate.in_active = num_attr.create('active', 'active', OpenMaya.MFnNumericData.kBoolean, False)
        num_attr.storable = True
        num_attr.keyable = True
        num_attr.channelBox = True
        AutoRotate.addAttribute(AutoRotate.in_active)

        AutoRotate.in_inv_rot = num_attr.create('invertRotation', 'invertRotation', OpenMaya.MFnNumericData.kBoolean, False)
        num_attr.storable = True
        num_attr.keyable = True
        num_attr.channelBox = True
        AutoRotate.addAttribute(AutoRotate.in_inv_rot)

        AutoRotate.in_scale = num_attr.create('scale', 'scale', OpenMaya.MFnNumericData.kFloat, 1.0)
        num_attr.setMin(0.01)  # Prevent ZeroDivisionError
        num_attr.storable = True
        num_attr.keyable = True
        num_attr.channelBox = True
        AutoRotate.addAttribute(AutoRotate.in_scale)

        AutoRotate.in_radius = num_attr.create('radius', 'radius', OpenMaya.MFnNumericData.kFloat, 1.0)
        num_attr.storable = True
        num_attr.keyable = True
        num_attr.channelBox = True
        AutoRotate.addAttribute(AutoRotate.in_radius)

        AutoRotate.in_aim_vec = num_attr.createPoint('aimVector', 'aimVector')
        num_attr.setDefault(1.0, 0.0, 0.0)
        num_attr.storable = True
        num_attr.keyable = True
        num_attr.channelBox = True
        AutoRotate.addAttribute(AutoRotate.in_aim_vec)

        AutoRotate.in_driver_matrix = matrix_attr.create('driverMatrix', 'driverMatrix')
        AutoRotate.addAttribute(AutoRotate.in_driver_matrix)

        AutoRotate.attributeAffects(AutoRotate.in_active, AutoRotate.out_rot)
        AutoRotate.attributeAffects(AutoRotate.in_scale, AutoRotate.out_rot)
        AutoRotate.attributeAffects(AutoRotate.in_radius, AutoRotate.out_rot)
        AutoRotate.attributeAffects(AutoRotate.in_inv_rot, AutoRotate.out_rot)
        AutoRotate.attributeAffects(AutoRotate.in_aim_vec, AutoRotate.out_rot)
        AutoRotate.attributeAffects(AutoRotate.in_driver_matrix, AutoRotate.out_rot)

    def __init__(self):
        OpenMayaMPx.MPxNode.__init__(self)
        self.prev_translation = OpenMaya.MVector()
        self.prev_rotation = 0.0
        self.prev_active = False

    def compute(self, plug, data):
        # Check that the requested recompute is one of the output values
        if plug != AutoRotate.out_rot:
            return OpenMaya.kUnknownParameter

        active = data.inputValue(AutoRotate.in_active).asBool()

        if active is False:
            self.prev_active = False
            return OpenMaya.kUnknownParameter
        scale = data.inputValue(AutoRotate.in_scale).asFloat()
        radius = data.inputValue(AutoRotate.in_radius).asFloat()
        inv_rot = data.inputValue(AutoRotate.in_inv_rot).asBool()
        aim_vec = data.inputValue(AutoRotate.in_aim_vec).asFloatVector()
        driver_matrix = data.inputValue(AutoRotate.in_driver_matrix).asMatrix()
        aim_vec_ws = OpenMaya.MVector(aim_vec) * driver_matrix
        translation_ws = OpenMaya.MTransformationMatrix(driver_matrix).getTranslation(OpenMaya.MSpace.kWorld)
        if self.prev_active is False:
            self.prev_translation = translation_ws
        move_vec = translation_ws - self.prev_translation
        move_delta = move_vec * aim_vec_ws  # moveVec.length()
        if inv_rot is True:
            rotation = self.prev_rotation + -move_delta * 360 / (2 * math.pi * radius * scale)
        else:
            rotation = self.prev_rotation + move_delta * 360 / (2 * math.pi * radius * scale)

        self.prev_rotation = rotation
        self.prev_translation = translation_ws

        rot_handle = data.outputValue(AutoRotate.out_rot)
        rot_handle.setFloat(rotation)

        self.prev_active = True
        data.setClean(plug)
        return None


def initializePlugin(mobject):
    mplugin = OpenMayaMPx.MFnPlugin(mobject, kPluginAuthor, kPluginVersion, "Any")
    try:
        mplugin.registerNode(kPluginName, kPluginId, AutoRotate.creator, AutoRotate.initialize)
    except:
        sys.stderr.write("Failed to register node: %s" % kPluginName)
        raise


def uninitializePlugin(mobject):
    mplugin = OpenMayaMPx.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(kPluginId)
    except:
        sys.stderr.write("Failed to deregister node: %s" % kPluginName)
        raise
